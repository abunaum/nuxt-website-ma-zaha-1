import { defineStore } from 'pinia';

export const useModalIcoStore = defineStore('modalico', {
    state: ():{isVisible:boolean, modalIsi:any} => ({
        isVisible: false,
        modalIsi:null
    }),
    actions: {
        showModal(isi:any) {
            this.modalIsi = isi;
            this.isVisible = true;
        },
        hideModal() {
            this.isVisible = false;
        }
    }
});

export const useModalMenuStore = defineStore('modalmenu', {
    state: ():{isVisible:boolean, isi:any, title: string} => ({
        isVisible: false,
        isi:"",
        title: ""
    }),
    actions: {
        showModal(title: string,isi:any) {
            this.isVisible = true;
            this.isi = isi;
            this.title = title;
        },
        hideModal() {
            this.isVisible = false;
            this.title = "";
            this.isi = "";
        }
    }
});