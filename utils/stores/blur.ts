import { defineStore } from 'pinia';

export const useBlurStore = defineStore('blur', {
    state: () => ({
        blur: false
    }),
    actions: {
        blurComponent() {
            this.blur = true;
        },
        clearBlur() {
            this.blur = false;
        }
    }
});