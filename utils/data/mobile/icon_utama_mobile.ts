export const ico1 = [
    {
        id: 1,
        src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav",
        text: "Program Madrasah"
    },
    {
        id: 2,
        src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/vw2kc2meixs2tjow6pad",
        text: "Platform Digital"
    },
    {
        id: 3,
        src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/tdvk5ywhlajaalzgjfpc",
        text: "Pelayanan Administrasi"
    },
    {
        id: 4,
        src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/d8b0y3kfkpfu5efdixjn",
        text: "Fasilitas"
    },
    {
        id: 5,
        src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/v8sxm5zhmjrff5o3e0gp",
        text: "Galeri"
    },
    {
        id: 6,
        src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/xiuzqqd29byfxqqzhxmo",
        text: "Ekstra Kulikuler"
    }
];

export const ico2 = [
  {
    id:7,
    src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/sctvb8ktatxpnaxvkr0i",
    text: "Media MA ZAHA 1"
  },
  {
    id:8,
    src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/f7skmhv9gpppqe0licgc",
    text: "Pengumuman"
  },
  {
    id:9,
    src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/eanjjlb4o1wi7mecq7oq",
    text: "PSB"
  },
  {
    id:10,
    src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/wyqo5espzmq7xn8vlbgr",
    text: "Live"
  },
  {
    id:11,
    src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/mmupnnck9vqyj8prdy0q",
    text: "MA ZAHA Dalam Angka"
  },
  {
    id:12,
    src: "https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/oj9fnxnieme5nuotxpbg",
    text: "Lokasi Madrasah"
  }
]