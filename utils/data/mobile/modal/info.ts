export default () => {
    return {
        title: 'Info Madrasah',
        isi: [
            {
                nama: 'Warta Madrasah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/INFO%20MADRASAH/t7n75u6t2bxdekmk8sfm',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/berita',
                }
            },
            {
                nama: 'Prestasi Santri',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/INFO%20MADRASAH/o8gsewxceg7soiozvo7h',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/berita/prestasi',
                }
            },
            {
                nama: 'Osis',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/INFO%20MADRASAH/z6xh4cjm9j63lobmg9oc',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/berita/osis',
                }
            },
            {
                nama: 'Alumni',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/INFO%20MADRASAH/qlqgxpaqkxznonmswnnj',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/berita/alumni',
                }
            },
            {
                nama: 'Opini',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/INFO%20MADRASAH/wy4wbojoabd3q7s2mpdl',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/berita/opini',
                }
            },
            {
                nama: "Kajian Keislaman",
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/INFO%20MADRASAH/nzdtgmezwhjrgmmb4bz7',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/berita/kajian_keislaman',
                }
            },
            {
                nama: 'Teacher Of The Week',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/INFO%20MADRASAH/owwzadbt7sb6l2ukelcx',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/berita/teacher_of_the_week',
                }
            }
        ]

    }
}