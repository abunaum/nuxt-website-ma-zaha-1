import kepala_madrasah from "~/components/pages/mobile/home/modal/kepala_madrasah_masa_ke_masa.vue";
import visi_misi from "~/components/pages/mobile/home/modal/visi_misi.vue";

export default () => {
    return {
        title: 'Profil Madrasah',
        isi: [
            {
                nama: 'Sambutan Kepala Madrasah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROFIL%20MADRASAH/hfkqh34bom0i3zkydj3v',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/sambutan',
                }
            },
            {
                nama: 'Profil Asatid',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROFIL%20MADRASAH/pnerlvig17lboqjtbyed',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Sejarah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROFIL%20MADRASAH/j2yeli2gqe5vv9pnzgke',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Visi & Misi Madrasah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROFIL%20MADRASAH/sjwrstyoagjaaf9pfyjc',
                onclick: {
                    type: 'modal',
                    component: visi_misi,
                }
            },
            {
                nama: 'Logo Madrasah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROFIL%20MADRASAH/lpjeg6duhx6f8srhcd28',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: "Kepala Madrasah Dari Masa Ke Masa",
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROFIL%20MADRASAH/tkdamzxwqvtpaktjfbgq',
                onclick: {
                    type: 'modal',
                    component: kepala_madrasah
                }
            }
        ]

    }
}