export default () => {
    return {
        title: 'PSB',
        isi: [
            {
                nama: 'Daftar',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/tpzox61qzf8sd90uucxq',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Cara Mendaftar',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/fgm6xfag8qcotd6f69ja',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Gelombang Pendaftaran',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/grxnhgdnnnhhzhjp71st',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Waktu Pendaftaran',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/l5isoftp3velhbelanpt',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Formulir',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/hqi91a2lot8uzjvejqwl',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Brosur',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/etzs65wjdman7xyik4d4',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Video Profil',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/w4auamvchlb3uivyuoon',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Panitia',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/s24ktldlfra2fkdgbior',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
            {
                nama: 'Sekertariat',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PSB/jm3ropqoydike6q8cljb',
                onclick: {
                    type: 'page',
                    url: 'https://ppsb.vercel.app/',
                }
            },
        ]

    }
}