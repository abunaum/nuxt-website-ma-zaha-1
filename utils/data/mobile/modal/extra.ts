export default () => {
    return {
        title: 'Ekstrakulikuler',
        isi: [
            {
                nama: 'Seni Hadrah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/lft5fe7d2oog6jh8cdss',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'MC',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/pp4b305npiushoqeagah',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Pramuka',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/td93rmi7wqhajljjeovu',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Karya Tulis Ilmiah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/uaqt819va3dhkmp07fqi',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Keputrian',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/xtnmkevda5tt9yec0udq',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: "Tartilul Qur'an",
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/x27ndcvi9mvnjiyd1ryf',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Tilawah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/yxnqzqzuvvry5vnh0zip',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Futsal',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/urnyydn5g8dubwgqzcuu',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Sepak Bola',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/pq0pgyn6aueus7teofnz',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Volly',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/viwpe3lpi77i8dyu58jn',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Bulutangkis',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/pbuwjydz04peksk8sbnt',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Tenis Meja',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/liu7cbxoazzlsekr8bk4',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Bela Diri',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/EKTRA/nfugtgwulkxcrwkcd389',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            }
        ]

    }
}