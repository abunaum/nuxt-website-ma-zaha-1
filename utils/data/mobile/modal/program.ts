import unggulan from "~/components/pages/mobile/home/modal/program_unggulan.vue"
import peminatan from "~/components/pages/mobile/home/modal/program_peminatan.vue"
import ekstra from "~/components/pages/mobile/home/modal/ekstra.vue"

export default () => {
    return {
        title: 'Program Madrasah',
        isi: [
            {
                nama: 'Program Peminatan',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROGRAM%20MADRASAH/pf5vguuerrv4u9tulads',
                onclick: {
                    type: 'modal',
                    component: peminatan,
                    url: '/',
                }
            },
            {
                nama: 'Program Unggulan',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',
                onclick: {
                    type: 'modal',
                    component: unggulan,
                    url: '/',
                }
            },
            {
                nama: 'Ekstra Kulikuler',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROGRAM%20MADRASAH/lupkvmp6h9vkjban1xq6',
                onclick: {
                    type: 'modal',
                    component: ekstra,
                    url: '/',
                }
            },
            {
                nama: 'Bidang Kurikulum',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROGRAM%20MADRASAH/dhvc0tg2d1dkvwgly6h4',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/kurikulum',
                }
            },
            {
                nama: 'Budang Kesiswaan',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROGRAM%20MADRASAH/cvkgaimkqiw2thcpfspz',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/kesiswaan',
                }
            },
            {
                nama: 'Bidang Humas',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROGRAM%20MADRASAH/teofarwpmsvkrppnz4cy',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/humas',
                }
            },
            {
                nama: 'Bidang Sarana & Prasarana',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PROGRAM%20MADRASAH/dz7ytqcd12b3qkhbyqlw',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/sarana_prasarana',
                }
            }
        ]

    }
}