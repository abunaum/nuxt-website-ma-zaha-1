export default () => {
    return {
        title: 'Fasilitas',
        isi: [
            {
                nama: 'Absensi Aromaza',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/FASILITAS/hplvse2tydc1exdnvyng',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Laboratorium Komputer',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/FASILITAS/bnf5ifnqu9lgdhedjwgk',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Laboratorium IPA',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/FASILITAS/tjjoapswfdrwqo7iacsp',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Wifi',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/FASILITAS/dchpvfo8ishnxc2o7pxn',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Smart TV',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/FASILITAS/shq7fvhuwoo3fccdgsea',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Proyektor',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/depan/sqvg8legj2tvnkuqeuor',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
        ]

    }
}