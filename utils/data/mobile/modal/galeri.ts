export default () => {
    return {
        title: 'Galeri',
        isi: [
            {
                nama: '2020',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/v8sxm5zhmjrff5o3e0gp',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/galeri/tahun/2020',
                }
            },
            {
                nama: '2021',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/v8sxm5zhmjrff5o3e0gp',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/galeri/tahun/2021',
                }
            },
            {
                nama: '2022',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/v8sxm5zhmjrff5o3e0gp',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/galeri/tahun/2022',
                }
            },
            {
                nama: '2023',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/v8sxm5zhmjrff5o3e0gp',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/galeri/tahun/2023',
                }
            },
            {
                nama: '2024',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/v8sxm5zhmjrff5o3e0gp',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/galeri/tahun/2024',
                }
            }
        ]

    }
}