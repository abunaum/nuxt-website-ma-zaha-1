export default () => {
    return {
        title: 'Pelayanan Administrasi',
        isi: [
            {
                nama: 'Bendahara',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/ADMINISTRASI/fn2wq9ndsfe0i6tqcact',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/pelayanan/bendahara',
                }
            },
            {
                nama: 'Putri',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/ADMINISTRASI/fn2wq9ndsfe0i6tqcact',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/pelayanan/administrasi_putri',
                }
            },
            {
                nama: 'Putra',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/ADMINISTRASI/fn2wq9ndsfe0i6tqcact',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/pelayanan/administrasi_putra',
                }
            },
        ]

    }
}