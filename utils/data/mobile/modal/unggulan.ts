export default () => {
    return {
        title: 'Program Unggulan',
        isi: [
            {
                nama: 'Prodistik',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',
                onclick: {
                    type: 'page',
                    url: '/galeri/prodistik',
                }
            },
            {
                nama: 'Tahqiqu Qiroatil Kutub',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Tahfidzul Qur\'an',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',

                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
        ]

    }
}