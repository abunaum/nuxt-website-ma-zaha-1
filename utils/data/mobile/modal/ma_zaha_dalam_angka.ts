import asatidz from "~/components/pages/mobile/home/modal/asatidz.vue";
import santri from "~/components/pages/mobile/home/modal/santri.vue";

export default () => {
    return {
        title: 'MA ZAHA Dalam Angka',
        isi: [
            {
                nama: 'Jurusan',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Asatid',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DALAM%20ANGKA/qtw6nwuf1ep8xxnkwyys',
                onclick: {
                    type: 'modal',
                    component: asatidz,
                }
            },
            {
                nama: 'Program Unggulan',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DALAM%20ANGKA/epxinihxmhdgukmiemnd',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Ekstrakulikuler',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DALAM%20ANGKA/iy5bgvfom5bn3j8yqzkw',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Santri',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DALAM%20ANGKA/fhwjz2pjgtca5vylw5pf',
                onclick: {
                    type: 'modal',
                    component: santri,
                }
            },
            {
                nama: 'Output Lulusan',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DALAM%20ANGKA/ecripcrhceqgpevuwiqs',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            }
        ]

    }
}