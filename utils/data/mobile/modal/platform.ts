export default () => {
    return {
        title: 'Platform Digital',
        isi: [
            {
                nama: 'Simumtaz',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DIGITAL/fv2z3w1qcmijlastbcf3',
                onclick: {
                    type: 'page',
                    url: 'https://simumtaz.mazainulhasan1.sch.id/',
                }
            },
            {
                nama: 'Aromaza',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DIGITAL/fv2z3w1qcmijlastbcf3',
                onclick: {
                    type: 'page',
                    url: 'https://t.me/MAzahaBot',
                }
            },
            {
                nama: 'Product Website tes psikologi',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DIGITAL/fv2z3w1qcmijlastbcf3',
                onclick: {
                    type: 'page',
                    url: 'https://psychic.mazainulhasan1.sch.id/',
                }
            },
            {
                nama: 'Gramazaha (Product Desain Grafis)',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/DIGITAL/fv2z3w1qcmijlastbcf3',
                onclick: {
                    type: 'page',
                    url: 'https://www.instagram.com/gramazaha/',
                }
            }
        ]

    }
}