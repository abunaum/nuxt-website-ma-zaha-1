export default () => {
    return {
        title: 'Live',
        isi: [
            {
                nama: 'Telegram',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/LIVE/mmdccicxadyxneriyyog',
                onclick: {
                    type: 'page',
                    url: 'https://t.me/ma_zahagenggong',
                }
            },
            {
                nama: 'Youtube',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/LIVE/okrbr9vjewnfkf5oy7yu',
                onclick: {
                    type: 'page',
                    url: 'https://www.youtube.com/@mazaha1genggong879/streams',
                }
            },
        ]

    }
}