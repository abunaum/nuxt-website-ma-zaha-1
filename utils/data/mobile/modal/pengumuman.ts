export default () => {
    return {
        title: 'Pengumuman',
        isi: [
            {
                nama: 'Akademik',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PENGUMUMAN/s5ikl80exrcwcv95c283',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/pengumuman/akademik',
                }
            },
            {
                nama: 'Non Akademik',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/PENGUMUMAN/s5ikl80exrcwcv95c283',
                onclick: {
                    type: 'page',
                    current: true,
                    url: '/pengumuman/non_akademik',
                }
            },
        ]

    }
}