export default () => {
    return {
        title: 'Media MA ZAHA',
        isi: [
            {
                nama: 'Berita Madrasah',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',
                onclick: {
                    type: 'page',
                    url: '/berita',
                }
            },
            {
                nama: 'Struktur Organisasi',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Profil Tim Media',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            },
            {
                nama: 'Agenda',
                image: 'https://res.cloudinary.com/dky2iwfj8/image/upload/f_auto,q_auto/v1/new-web/img/bahan/mobile/epbxeen9i1egu83etrav',
                onclick: {
                    type: 'page',
                    url: '/',
                }
            }
        ]

    }
}