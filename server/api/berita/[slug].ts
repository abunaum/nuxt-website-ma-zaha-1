import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();
export default defineEventHandler(async (event) => {
    const slug = getRouterParam(event, 'slug');
    try {
        const data = await prisma.blog.findFirst({
            where: {
                slug: slug,
            }
        });
        if (!data) {
            return {
                success: false,
                message: "Data tidak ditemukan"
            }
        }
        return {
            success: true,
            data
        }
    } catch (error) {
        return {
            success: false,
            message: "Koneksi bermasalah"
        }
    }
})