import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export default defineEventHandler(async (event) => {
    try {
        const body = await readBody(event);
        const page = parseInt(body.page || 1);
        const jumlah = parseInt(body.jumlah || 10);

        const skip = (page - 1) * jumlah;

        // Menghitung jumlah total data
        const totalData = await prisma.blog.count();

        // Menghitung total halaman (totalData dibagi jumlah per halaman)
        const totalPages = Math.ceil(totalData / jumlah);
        let data;
        if (body?.kategori){
            data = await prisma.blog.findMany({
                orderBy: {
                    createdAt: 'desc',
                },
                skip: skip,
                take: jumlah,
                where: {
                    category: {
                        name: body.kategori
                    }
                }
            });
        } else {
            data = await prisma.blog.findMany({
                orderBy: {
                    createdAt: 'desc',
                },
                skip: skip,
                take: jumlah,
            });
        }
        return {
            success: true,
            data: {
                totalData: totalData,  // Jumlah total data
                totalPages: totalPages,  // Total halaman
                currentPage: page,  // Halaman saat ini
                jumlah: jumlah,  // Jumlah per halaman
                data: data,  // Data blog sesuai dengan pagination
            },
            message: "Data berhasil didapatkan",
        };
    } catch (error) {
        setResponseStatus(event, 501);
        return {
            success: false,
            message: "Data gagal didapatkan",
            error: error,
        };
    }
});
