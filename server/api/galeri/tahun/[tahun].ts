import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export default defineEventHandler(async (event) => {
    const tahun = decodeURIComponent(getRouterParam(event, 'tahun')?? "undefined");
    try {
       const galeri = await prisma.galeri.findMany({
           where: {
               subCategory:{
                   tahun
               }
           },
           include: {
               category: true,
               subCategory: true
           }
       })
        return {
            success: true,
            data: galeri,
        }
    } catch (error) {
        console.error(error)
        return {
            success: false,
            message: "Gagal mengambil data"
        }
    }
});