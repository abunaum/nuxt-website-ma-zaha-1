import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export default defineEventHandler(async (event) => {
    const name = decodeURIComponent(getRouterParam(event, 'name')?? "undefined");
    try {
        const sub = await prisma.galeri_Sub_Kategori.findFirst({
            where: {
                name
            }
        })
       const galeri = await prisma.galeri.findMany({
           where: {
               subCategory:{
                   name
               }
           },
           include: {
               category: true,
               subCategory: true
           }
       })
        return {
            success: true,
            data: galeri,
            sub
        }
    } catch (error) {
        console.error(error)
        return {
            success: false,
            message: "Gagal mengambil data prodistik"
        }
    }
});