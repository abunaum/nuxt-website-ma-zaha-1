import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export default defineEventHandler(async (event) => {
    try {
        const subKategori = await prisma.galeri_Sub_Kategori.findMany({
            where: {
                galeris: {
                    some: {
                        category: {
                            name: "Prodistik"
                        }
                    }
                }
            },
            include: {
                galeris: {
                    include: {
                        category: true // Opsional, jika ingin melihat data kategorinya juga
                    }
                }
            }
        });
        return {
            success: true,
            data: subKategori
        }
    } catch (error) {
        console.error(error)
        return {
            success: false,
            message: "Gagal mengambil data prodistik"
        }
    }
});