import {PrismaClient} from '@prisma/client';

const prisma = new PrismaClient()
export default defineEventHandler(async (event) => {
    try{
        const data = await prisma.kepalaMadrasah.findMany({
            orderBy: {
                urutan: 'asc'
            }
        })
        return {
            success: true,
            data: data,
            message: "data berhasil didapatkan"
        }
    } catch (error) {
        setResponseStatus(event, 501);
        return {
            success: false,
            message: "data gagal didapatkan",
            error: error
        }
    }
})