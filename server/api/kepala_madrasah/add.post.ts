import {PrismaClient} from '@prisma/client';

const prisma = new PrismaClient()
export default defineEventHandler(async (event) => {
    const body = await readBody(event);
    if (!body) {
        setResponseStatus(event, 400)
        return {
            success:false,
            message: "data kosong"
        }
    }
    if (!body.urutan || !body.image) {
        setResponseStatus(event, 400)
        return {
            success:false,
            message: "data tidak lengkap"
        }
    }
    try{
        const simpan = await prisma.kepalaMadrasah.create({
            data:{
                urutan: body.urutan,
                image: body.image,
            }
        })
        return {
            success:true,
            message: "kepala madrasah berhasil ditambah",
            data: simpan
        }
    } catch (error) {
        return {
            success:false,
            message: "kepala madrasah gagal ditambah",
            error
        }
    }
})